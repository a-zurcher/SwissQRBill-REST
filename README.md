# Swiss QR-Bill REST Service

![](https://gitlab.com/a-zurcher/SwissQRBill-REST/badges/main/pipeline.svg)

Uses [SwissQRBill](https://github.com/manuelbl/SwissQRBill) Java library, and [Ktor](https://ktor.io/) for REST API. Written in Kotlin.

## Installation

Create a podman/docker container with port 8080 opened :

```bash
podman run --name swissqrbill-rest -p 8080:8080 azurcher/swissqrbill-rest
```

## Usage

Access the local Swagger UI at `/openapi` (http://localhost:8080/openapi).

---

Example curl request:

```bash
curl --request GET \
  --url http://localhost:8080/qr \
  --header 'Content-Type: application/json' \
  --output qr-bill.svg \
  --data '{
  "amount": 1000,
  "currency": "CHF",
  "unstructuredMessage": "Your_message_here",
  "debtor": {
    "name": "Name",
    "street": "Street",
    "houseNo": "100",
    "countryCode": "CH",
    "postalCode": "0000",
    "town": "Town"
  },
  "format": {
    "graphicsFormat": "SVG",
    "outputSize": "QR_BILL_EXTRA_SPACE",
    "language": "FR"
  },
  "account": "CH2108307000426495306",
  "creditor": {
    "name": "Name",
      "street": "Street",
      "houseNo": "100",
      "countryCode": "CH",
      "postalCode": "0000",
      "town": "Town"
  },
  "billInformation": "",
  "reference": ""
}'
```

Which generates the following SVG image :

![](qr-bill.svg)

---

Here is the expected JSON structure :

```json
{
  "amount": 1000,
  "currency": "CHF",
  "unstructuredMessage": "Your_message_here",
  "debtor": {
    "name": "Name",
    "street": "Street",
    "houseNo": "100",
    "countryCode": "CH",
    "postalCode": "0000",
    "town": "Town"
  },
  "format": {
    "graphicsFormat": "SVG",
    "outputSize": "QR_BILL_EXTRA_SPACE",
    "language": "FR"
  },
  "account": "CH2108307000426495306",
  "creditor": {
    "name": "Name",
    "street": "Street",
    "houseNo": "100",
    "countryCode": "CH",
    "postalCode": "0000",
    "town": "Town"
  },
  "billInformation": "",
  "reference": ""
}
```


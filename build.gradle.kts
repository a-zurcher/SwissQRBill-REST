val ktor_version: String by project
val kotlin_version: String by project
val logback_version: String by project

plugins {
    kotlin("jvm") version "1.9.20"
    id("io.ktor.plugin") version "2.3.6"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.6.10"
}

ktor {
    docker {
        localImageName.set("swissqrbill-rest")
        jreVersion.set(JavaVersion.VERSION_1_9)
        io.ktor.plugin.features.DockerImageRegistry.dockerHub(
            appName = provider { "ktor-app" },
            username = providers.environmentVariable("DOCKER_HUB_USERNAME"),
            password = providers.environmentVariable("DOCKER_HUB_PASSWORD")
        )
    }
}

group = "digital.zurcher"
version = "0.0.2"

application {
    mainClass.set("digital.zurcher.ApplicationKt")

    val isDevelopment: Boolean = project.ext.has("development")
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=$isDevelopment")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("io.ktor:ktor-server-servlet-jakarta:$ktor_version")
    implementation("io.ktor:ktor-server-core-jvm:2.3.1-eap-676")
    implementation("io.ktor:ktor-server-content-negotiation-jvm:2.3.1-eap-678")
    implementation("io.ktor:ktor-server-swagger-jvm:2.3.1-eap-676")
    implementation("io.ktor:ktor-server-netty-jvm:2.3.1-eap-677")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("net.codecrete.qrbill:qrbill-generator:3.1.0")
    implementation("io.ktor:ktor-serialization-kotlinx-json:2.3.2")
    testImplementation("io.ktor:ktor-server-tests-jvm:2.3.0-eap-622")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$kotlin_version")
}

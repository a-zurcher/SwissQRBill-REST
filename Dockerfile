# https://ktor.io/docs/docker.html#name-tag

FROM gradle:8.4-jdk17-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle buildFatJar --no-daemon

FROM openjdk:17
EXPOSE 8080:8080
RUN mkdir /app
COPY --from=build /home/gradle/src/build/libs/*.jar /app/swissqrbill-rest.jar
ENTRYPOINT ["java","-jar","/app/swissqrbill-rest.jar"]
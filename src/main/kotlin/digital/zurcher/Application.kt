package digital.zurcher

import digital.zurcher.plugins.configureHTTP
import digital.zurcher.plugins.configureRouting
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import kotlinx.serialization.json.Json

fun main(args: Array<String>): Unit = EngineMain.main(args)

fun Application.module() {
    configureHTTP()
    configureRouting()
    install(ContentNegotiation) {
        json(Json {
            prettyPrint = true
        })
    }
}

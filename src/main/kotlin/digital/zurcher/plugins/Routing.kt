package digital.zurcher.plugins

import digital.zurcher.qrbill.SerializableBill
import digital.zurcher.qrbill.deserializeBill
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import net.codecrete.qrbill.generator.*

fun Application.configureRouting() {
    routing {
        // TODO add error mgmt
        get("/qr") {
            val bill = deserializeBill(call.receive<SerializableBill>())
            val svg: ByteArray = QRBill.generate(bill)
            call.respondBytes(svg, ContentType.Image.SVG, HttpStatusCode.OK)
        }
    }
}

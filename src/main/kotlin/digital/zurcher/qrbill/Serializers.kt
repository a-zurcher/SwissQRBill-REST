package digital.zurcher.qrbill

import kotlinx.serialization.Serializable
import net.codecrete.qrbill.generator.*

fun deserializeBill(sBill: SerializableBill): Bill {
    val bill = Bill()

    bill.account = sBill.account
    bill.setAmountFromDouble(sBill.amount)
    bill.currency = sBill.currency

    bill.creditor = deserializeAddress(sBill.creditor)
    bill.debtor = deserializeAddress(sBill.debtor)

    bill.reference = sBill.reference
    bill.unstructuredMessage = sBill.unstructuredMessage

    bill.format = deserializeBillFormat(sBill.format)

    return bill
}

fun deserializeAddress(sAdress: SerializableAddress): Address {
    val address = Address()
    address.name = sAdress.name
    address.addressLine1 = "${sAdress.street} ${sAdress.houseNo}"
    address.addressLine2 = "${sAdress.postalCode} ${sAdress.town}"
    address.countryCode = sAdress.countryCode

    return address
}

fun deserializeBillFormat(sBillFormat: SerializableBillFormat): BillFormat {
    val format= BillFormat()

    format.graphicsFormat = sBillFormat.graphicsFormat
    format.outputSize = sBillFormat.outputSize
    format.language = sBillFormat.language

    return format
}

@Serializable
data class SerializableBill(
    val account: String,
    val amount: Double,
    val currency: String,
    val creditor: SerializableAddress,
    val debtor: SerializableAddress,
    val format: SerializableBillFormat,
    val unstructuredMessage: String? = "",
    val reference: String? = "",
    val billInformation: String? = "",
)

@Serializable
data class SerializableAddress(
    val name: String,
    val street: String,
    val houseNo: String,
    val postalCode: String,
    val town: String,
    val countryCode: String = "CH"
)

@Serializable
data class SerializableBillFormat(
    val graphicsFormat: GraphicsFormat,
    val outputSize: OutputSize,
    val language: Language
)